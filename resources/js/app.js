require('./bootstrap')
import 'es6-promise/auto'
import axios from 'axios'
import auth from './auth'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'


Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(VueAxios, axios)
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`

import App from './components/App'
import Home from './components/Home'
import View from './components/View'
import Manage from './components/Manage'
import History from './components/History'
import Login from './components/Login'
import Register from './components/Register'
import Create from './components/Create'
import Edit from './components/Edit'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                auth: undefined
            }
        },
        {
            path: '/view',
            name: 'view',
            component: View,
            meta: {
                auth: true
            }
        },
        {
            path: '/manage',
            name: 'manage',
            component: Manage,
            meta: {
                auth: true
            }
        },
        {
            path: '/history',
            name: 'history',
            component: History,
            meta: {
                auth: true
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/edit/:id',
            name: 'edit',
            component: Edit,
            meta: {
                auth: true
            }
        },
        {
            path: '/create',
            name: 'create',
            component: Create,
            meta: {
                auth: true
            }
        }
    ],
});

Vue.router = router
Vue.use(VueAuth, auth)

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    vuetify: new Vuetify()
});
