<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['name', 'phone', 'email', 'birthday', 'address', 'photo', 'user_id'];

    public function changes()
    {
        return $this->hasMany('App\Change');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
