<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ContactCollection;
use App\Http\Resources\ChangeCollection;
use App\Contact;
use Intervention\Image\Facades\Image;
use App\Change;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $contact = new Contact();

        $contact->fill($request->all());

        $contact->user_id = Auth::user()->id;

        if ($request->get('photo')) {
            $photo = $request->get('photo');
            $name = time() . '.' . explode('/', explode(':', substr($photo, 0, strpos($photo, ';')))[1])[1];
            $dir = public_path('photos/');
            if (!file_exists($dir)) {
                mkdir($dir, 0755, true);
            }
            Image::make($request->get('photo'))->save($dir.$name);
            $contact->photo = '/photos/'.$name;
        }

        $contact->save();

        Change::create([
           'log'          => 'Contact with phone: '.$contact->phone.' was created',
           'contact_id'  => $contact->id
        ]);

        return response()->json('successfully added');
    }

    public function index()
    {
        $userID   = Auth::user()->id;
        $contacts = Contact::where('user_id', $userID)->get();
        return new ContactCollection($contacts);
    }

    public function edit($id)
    {
        $contact = Contact::find($id);
        return response()->json($contact);
    }

    public function update($id, Request $request)
    {
        $contact = Contact::find($id);

        $contact->fill($request->all());

        if ($request->get('photo')) {
            $photo = $request->get('photo');
            $name = time() . '.' . explode('/', explode(':', substr($photo, 0, strpos($photo, ';')))[1])[1];
            $dir = public_path('photos/');
            if (!file_exists($dir)) {
                mkdir($dir, 0755, true);
            }
            Image::make($request->get('photo'))->save($dir.$name);
            $contact->photo = '/photos/'.$name;
        }

        foreach ($contact->getChanges() as $key => $value) {
            $original = $contact->getOriginal($key);
            Change::create([
               'log'        => "Value of field {$key} was changed from {$original} to {$value}",
               'contact_id' => $contact->id
            ]);
        }

        $contact->save();

        return response()->json('successfully updated');
    }

    public function delete($id)
    {
        Contact::destroy($id);

        return response()->json('successfully deleted');
    }

    public function changes()
    {
        $user = Auth::user();
        $changes = $user->contacts()
                            ->join('changes', 'contacts.id', '=', 'changes.contact_id')
                            ->select('changes.*');
        return new ChangeCollection($changes);
    }
}
