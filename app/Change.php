<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Change extends Model
{
    protected $fillable = ['log', 'success', 'contact_id'];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }
}
